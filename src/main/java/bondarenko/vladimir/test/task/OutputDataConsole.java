package bondarenko.vladimir.test.task;

import java.util.Formatter;
import java.util.List;

public class OutputDataConsole implements OutputData<Entity> {

    @Override
    public void writeData(List<Entity> list) {
        int number = 0;
        for (Entity entity : list) {
            Formatter fmt = new Formatter();
            fmt.format("%-4d| %-15d| %-22s", ++number,
                    entity.getAmount(), entity.getPath());
            System.out.println(fmt);
            fmt.close();
        }
    }

}
