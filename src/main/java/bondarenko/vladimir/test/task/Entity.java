package bondarenko.vladimir.test.task;

public class Entity {

    private String path;
    private int amount;

    public Entity() {
    }

    public Entity(String path, int amount) {
        this.path = path;
        this.amount = amount;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
