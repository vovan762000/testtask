package bondarenko.vladimir.test.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Utils {

    static List<Entity> entitys;

    public static void filesCount(String path) {
        int counter = 0;
        Stack<File[]> stack = new Stack<>();
        File file = new File(path);
        File[] list = file.listFiles();
        stack.push(list);
        while (!stack.empty() && !StopPoint.interraptCount) {
            File[] tmplist = stack.pop();

            for (int i = 0; i < tmplist.length; i++) {
                if (!tmplist[i].isDirectory()) {
                    counter++;
                }
                if (tmplist[i].isDirectory()) {
                    stack.push(tmplist[i].listFiles());
                }
            }
        }
        entitys.add(new Entity(path, counter));
    }

    public static List<String> getReadFile(String fileToRead) {

        List<String> pathsList = new ArrayList<>();

        try (FileReader file = new FileReader(fileToRead);
                BufferedReader bufferedReader = new BufferedReader(file)) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                File currentLine = new File(line);
                if (currentLine.isDirectory()) {
                    pathsList.add(line);
                }
            }
        } catch (IOException e) {
            pathsList = null;
            //error message
        }
        return pathsList;
    }
}
