
package bondarenko.vladimir.test.task;

import java.util.List;

public interface OutputData<T> {
    public void writeData(List<T> list);
}
