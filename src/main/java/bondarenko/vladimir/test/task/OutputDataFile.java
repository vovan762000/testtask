package bondarenko.vladimir.test.task;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

public class OutputDataFile implements OutputData<Entity> {

    private String path;

    public OutputDataFile(String path) {
        this.path = path;
    }

    @Override
    public void writeData(List<Entity> EntityList) {
        final String[] header = new String[]{"path", "amount"};
        final CellProcessor[] processors = new CellProcessor[]{
            new NotNull(), // path
            new Optional(), // amount files
        };

        try (ICsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(path),
                CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE)) {

            // write the header
            beanWriter.writeHeader(header);

            for (Entity entity : EntityList) {
                beanWriter.write(entity, header, processors);
            }
        } catch (IOException e) {
            System.err.println("oops,IOException");
        }
    }

}
