package bondarenko.vladimir.test.task;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class StopPoint implements NativeKeyListener {

    static public boolean interraptCount;

    public StopPoint() {
        GlobalScreen.addNativeKeyListener(this);
        try {
            // register global key listener
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nke) {
        if (nke.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            interraptCount = true;
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nke) {
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nke) {
    }

    public static void closeNativeHook() throws NativeHookException {
        GlobalScreen.unregisterNativeHook();
    }
}
