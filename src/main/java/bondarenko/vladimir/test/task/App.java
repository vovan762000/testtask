package bondarenko.vladimir.test.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jnativehook.NativeHookException;

public class App {

    static OutputData<Entity> outputData;
    static List<String> list;

    public static void main(String[] args) throws IOException, NativeHookException, InterruptedException {
        if ((args != null) && (args.length == 2)) {
        list = Utils.getReadFile(args[0]);
        Utils.entitys = Collections.synchronizedList(new ArrayList());
        outputData = new OutputDataFile(args[1]);
        } else {
            System.out.println("Incorrect command line paramethers!");
        System.exit(0);
        }
        new StopPoint();

        for (String path : list) {
            Count count = new Count(path);
            count.start();
            count.join();
        }
        StopPoint.closeNativeHook();
        
        outputData.writeData(Utils.entitys);
        outputData = new OutputDataConsole();
        outputData.writeData(Utils.entitys);

    }
}
